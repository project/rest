<?php
/**
 * @file
 * Additional functionality related to taxonomy for use by the rest module.
 */

/**
 * Create a vocabulary.
 * 
 * @param $name
 * 		the name to assign to the vocabulary
 * @param $description
 * 		the descrption to use for the vocabulary
 * @param $types
 * 		an array of node types that can be tagged with terms in this vocabulary
 * @param $multiple
 * 		whether to allow selection of multiple terms from this vocabulary (default TRUE)
 * @param $required
 * 		whether selection of at least one term from this vocabulary is required for the node types that use it (default FALSE)
 * @param $hierarchy
 * 		0 == no hierarchy, 1 == single-level hierarchy, 2 == multi-level hierarchy of terms (default 2)
 * @param $relations
 * 		whether to allow related terms in this vocabulary (default FALSE)
 * @param $tags
 * 		whether to allow free tagging in this vocabulary (default FALSE)
 * @param $help
 * 		the help text to use when displaying the list of terms in this vocabulary for selection. (default blank)
 * 
 * @return the created vocabulary
 */
function taxonomy_create_vocabulary($name, $description, $types, $multiple = TRUE, $required = FALSE, $hierarchy = 2, $relations = FALSE, $tags = FALSE, $weight = 0, $help = '') {
  $edit = array(
  	'name' => $name,
  	'description' => $description,
  	'help' => $help,
  	'multiple' => $multiple,
    'required' => $required,
    'hierarchy' => $hierarchy,
    'relations' => $relations,
    'tags' => $tags,
    'weight' => $weight,
    'nodes' => array_combine($types, $types),
  );
  taxonomy_save_vocabulary($edit);
  return taxonomy_get_vocabulary_by_name($name);
}

/**
 * Retrieve a taxonomy by name.
 * 
 * @param $name
 * 		the name of the vocabulary sought
 * @see taxonomy_get_term_by_name
 * @return the vocabulary object sought
 */
function taxonomy_get_vocabulary_by_name($name) {
  $db_result = db_query(db_rewrite_sql("SELECT v.vid, v.* FROM {vocabulary} v WHERE LOWER('%s') LIKE LOWER(v.name)", 'v', 'vid'), trim($name));
  if ($db_result) {
    return db_fetch_object($db_result);
  }
  return FALSE;
}

/**
 * Add a term to a given vocabulary.
 * 
 * @param $vocabulary
 * 		the vocabulary to which to add the term
 * @param $term
 * 		the term to add
 * @return the term that was added
 * 
 * TODO: Error handling
 */
function taxonomy_add_term_to_vocabulary($vocabulary, $term) {
  $vocabulary_object = taxonomy_get_vocabulary_by_name($vocabulary);
  $edit = array(
    'name' => $term,
    'vid' => $vocabulary_object->vid,
  );
  taxonomy_save_term($edit);
  return taxonomy_get_term_from_vocabulary($vocabulary_object, $term);
}

/**
 * Retrieve a term object from a vocabulary object.
 * 
 * @param $vocabulary_object
 * @param $term
 * 
 * @return term name
 */
function taxonomy_get_term_from_vocabulary($vocabulary_object, $term) {
  $vid = $vocabulary_object->vid;
  foreach (taxonomy_get_term_by_name($term) as $term_object) {
    if ($term_object->vid == $vid) {
      return $term_object;
    }
  }
}