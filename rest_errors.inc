<?php
/**
 * @file
 * Collects the error notification logic of the REST module.
 * 
 * TODO: Make error messages themeable.
 */

/**
 * Send a response indicating that no content types were
 * available to satisfy a request, and terminate processing.
 */
function _rest_no_content_type_error() {
  drupal_set_header("HTTP/1.1 406 Not Acceptable");
  _rest_exit_with_error('No content types are available to satisfy this request.');
}

/**
 * Send a response indicating that one or more parameters
 * are missing, and terminate processing.
 * 
 * @param $parameters
 * 		name(s) of missing parameter(s)
 */
function _rest_missing_parameters_error($parameters) {
  drupal_set_header("HTTP/1.1 400 Bad Request");
  _rest_exit_with_error('Missing parameter' . (count($parameters) > 1 ? 's' : '') . ': ' . implode(', ', $parameters) . '.');
}

/**
 * Exit processing with the given error message,
 * also printing it to the error log.
 * 
 * @param $msg
 */
function _rest_exit_with_error($msg) {
  error_log($msg);
  print "$msg\n";
  exit();
}