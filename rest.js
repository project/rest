/**
 * Javascript functions for the REST module.
 *
 * @author Noel Bush
 */

if (Drupal.jsEnabled) {
	var REST = REST || {};
}

/**
 * Things to do when the document is ready.
 */
$(document).ready(function(){
   // Update the list of parameters and path elements for user function arguments (if any).
   REST.update_parameter_list();
   REST.update_path_element_list();
});

/**
 * Add a row of the indicated type.
 * This depends on elements being named and laid out in a
 * particular way (TODO: Document how!)
 */
REST.add_row = function(row_type) {
	if (!Drupal.jsEnabled) {
		return false;
	}
	// Get a copy of the row template.
	template = $('#' + row_type + 's > div.row-template').clone(true);

	// Remove the class that makes it invisible.
	template.removeClass('row-template');
	
	// Add the class that makes it display block-wise.
	template.addClass('row');
	
	// Compute the index of this new item.
	index = $('#' + row_type + 's > div.row').length;

	// Replace the 'XindexX' marker in name and id attributes of this div, and the input and label elements, with the index.
	template.attr('id', template.attr('id').replace(/XindexX/, index));
	template.html(template.html().replace(/XindexX/g, index));

	// Insert the new row above the 'add' button.
	$('#edit-' + row_type + 's-add-button').before(template);
	
	// Focus and select the row name field.
	$('#edit-' + row_type + 's-' + index + '-name')[0].select();
}

/**
 * Remove a row of the given type.
 * TODO: Document required naming/layout.
 */
REST.remove_row = function(row_type, index) {
	if (!Drupal.jsEnabled) {
		return false;
	}
	$('#' + row_type + '-' + index + '-row').remove();
	
	// Update the user argument values.
	REST.update_parameter_list();
}

/**
 * Change enabled/disabled status of
 * fields used to define translation method.
 */
REST.set_translation_method = function(method) {
	if (!Drupal.jsEnabled) {
		return false;
	}
	if (method == 'uri') {
		REST.toggle_enabled_fields('#edit-translated-uri', new Array('#edit-user-function'));
	} else if (method == 'user function') {
		REST.toggle_enabled_fields('#edit-user-function', new Array('#edit-translated-uri'));
	}
}

/**
 * Change enabled/disabled status of
 * fields used to define the value type of a user function argument.
 */
REST.set_user_argument_value_type = function(index, type) {
	if (!Drupal.jsEnabled) {
		return false;
	}
	if (type == 'path element') {
		REST.toggle_enabled_fields('#edit-user-function-arguments-' + index + '-path-element-value', 
			new Array('#edit-user-function-arguments-' + index + '-parameter-value',
						    '#edit-user-function-arguments-' + index + '-string-value'));
	} else if (type == 'parameter') {
		REST.toggle_enabled_fields('#edit-user-function-arguments-' + index + '-parameter-value',
			new Array('#edit-user-function-arguments-' + index + '-path-element-value',
						    '#edit-user-function-arguments-' + index + '-string-value'));
	} else if (type == 'string') {
		REST.toggle_enabled_fields('#edit-user-function-arguments-' + index + '-string-value',
			new Array('#edit-user-function-arguments-' + index + '-path-element-value',
						    '#edit-user-function-arguments-' + index + '-parameter-value'));
	}
}

/**
 * Set the field with the id of _enable_ to enabled,
 * and fields with ids in _disable_ to disabled.
 *
 * @param enable a string id
 * @param disable an array of string ids
 */
REST.toggle_enabled_fields = function(enable, disable) {
	if (!Drupal.jsEnabled) {
		return false;
	}
	$(enable).removeAttr('disabled').end();
	for (index in disable) {
		$(disable[index]).attr('disabled', 'disabled');
	}
}

/**
 * Update the list of available parameters for user function arguments,
 * and set the option list for every user function argument
 * parameter select field on the page to this list.
 */
REST.update_parameter_list = function() {
	if (!Drupal.jsEnabled) {
		return false;
	}
	REST.update_user_function_argument_value_list('parameter', REST.get_parameters_as_option_list);
}

/**
 * Update the list of available path elements for user function arguments,
 * and set the option list for every user function argument
 * parameter select field on the page to this list.
 */
REST.update_path_element_list = function() {
	if (!Drupal.jsEnabled) {
		return false;
	}
	REST.update_user_function_argument_value_list('path-element', REST.get_path_elements_as_option_list);
}

/**
 * Update the list of available values for a user function argument,
 * and set the option list for every user function argument
 * parameter select field on the page to this list.
 *
 * @param item_type what type of item (which select) is getting updated
 * @param get_options the function to return available options
 */
REST.update_user_function_argument_value_list = function(item_type, get_options) {
	if (!Drupal.jsEnabled) {
		return false;
	}
	// Get any user function arguments.
	arguments = $('.user-function-argument-' + item_type + '-select');
	
	// If there are none, there's nothing more to do here.
	if (arguments.length == 0) {
		return;
	}
	
	// Create the list to hold the options, and gather them.
	options = get_options();

	hide_type = (options.length == 0);

	/* Iterate over all user function arguments and add options
	 * (or hide selects if there are no options. */
	arguments.each(function(i) {
		argument = $(this);
		if (hide_type) {
			//argument.hide();
		}
		else {
			//argument.removeAttr('display');
			selected = false;
			argument.children('option[@selected]').each(function(i) {
				selected = this.attr('value');
			}).end();
			argument.children('option').remove().end();
			for (index in options) {
				option = options[index].clone(true);
				if (selected && option.attr('value') == selected) {
					option.attr('selected', 'selected');
				}
				argument.append(option);
			}
		}
	});
	
	// Hide or display the radio button based on option count.
	$('.user-function-argument-' + '-value-type-radio').each(function(i) {
		if (hide_type) {
			//this.hide();
		}
		else {
			//this.removeAttr('display');
		}
	});
}

/**
 * Return the list of available parameters as an HTML option list.
 */
REST.get_parameters_as_option_list = function() {
	// Count parameters.
	parameter_count = $('#parameters > div.row').length;
	options = new Array();
	for (index = 0; index < parameter_count; index++) {
		value = $('#edit-parameters-' + index + '-name').attr('value');
		options.push($('<option value="' + value + '">' + value + '</option>'));
	}
	return options;
}

/**
 * Return the list of available path elements as an HTML option list.
 */
REST.get_path_elements_as_option_list = function() {
	options = new Array();
	path_element_regexp = new RegExp("\\[(.+?)\\]", 'g');
	while ((match = path_element_regexp.exec($('#edit-request-uri').attr('value'))) != undefined) {
		options.push($('<option value="' + match[1] + '">' + match[1] + '</option>'));
	}
	return options;
}
