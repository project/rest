<?php
/**
 * Additional functions for manipulating users, useful specifically to
 * the REST API.
 * 
 * WARNING!  These are dangerous functions!  They are used to "fake out"
 * Drupal's normal authentication mechanisms and allow the REST API to
 * act on behalf of any user.
 */