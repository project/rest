<?php
/**
 * @file
 * Generic utility functions that don't pertain to RESTfulness per se.
 * If equivalents to these exist in PHP or Drupal, they should be
 * preferred to the use of the following.
 */

// cache keys (variable names)
define('REST_CK_AVAILABLE_FUNCTIONS',          'rest_available_functions');

/**
 * Look for a value cached (as a Drupal variable) with the given $key.  If
 * none is found, use the supplied function to produce
 * the value and cache it with the key.  In either case,
 * return the value.
 * 
 * @param $key
 * @param $function
 * 		the function to call to get the value if it isn't cached
 * @param $arguments
 * 		arguments to the function for getting the value when not cached
 */
function _rest_util_get_cached($key, $function, $arguments = NULL) {
  $value = variable_get($key, FALSE);
  if (!$value) {
    $value = call_user_func_array($function, $arguments);
    variable_set($key, $value);
  }
  return $value;
}

/**
 * Produce an imploded view of the values in the given array associated
 * with the given $name.  Surround each value with $prefix and $suffix.
 * Use $separator as implode separator.
 */
function implode_named($array, $name, $prefix = '', $suffix = '', $separator) {
  error_log("array: " . print_r($array, TRUE));
  if ($array) {
		foreach ($array as $item) {
		  $items[] .= $prefix . $item[$name] . $suffix;
		}
		if (isset($items)) {
		  return implode($separator, $items);
		}
  }
}

/**
 * Call a user function with named parameters.
 * 
 * @param $function function name
 * @param $arguments
 * 
 * @see http://www.php.net/manual/en/function.call-user-func-array.php#66121 
 */
function call_user_func_named($function, $arguments) {
  $reflect = new ReflectionFunction($function);
	$real_parameters = array();
	foreach ($reflect->getParameters() as $index => $parameter) {
	    $parameter_name = $parameter->getName();
	    if ($parameter->isPassedByReference()) {
	      error_log("Parameter $parameter_name is passed by reference.");
	    }
	    if (array_key_exists($parameter_name, $arguments)) {
	        $real_parameters[] = $arguments[$parameter_name];
	    }
	    else if ($parameter->isDefaultValueAvailable()) {
	        $real_parameters[] = $parameter->getDefaultValue();
	    }
	    else {
	        // missing required parameter: mark an error and exit
	        trigger_error(sprintf('Call to %s missing parameter nr. %d', $function, $i + 1), E_USER_ERROR);
	    }
	}
	call_user_func_array($function, $real_parameters);
}

/**
 * Given an HTTP request body, as sent by a PUT request, parse the
 * assigned values of parameters.  (Probably PHP should supply
 * this functionality, but doesn't for PUT requests.)
 */
function http_parse_form_params($body) {
  preg_match_all('/Content\-Disposition: form\-data; name="(.+)"\r\n\r\n(.+)\r\n/', $body, $matches, PREG_PATTERN_ORDER);
  return array_combine($matches[1], $matches[2]);
}

/**
 * Return the set of available PHP functions in a format
 * suitable for building an option list, caching this
 * if is not already.
 */
function _rest_get_available_functions() {
  return _rest_util_get_cached(REST_CK_AVAILABLE_FUNCTIONS, '__rest_lookup_available_functions');
}

/**
 * Rebuild and cache the set of available functions.
 */
function _rest_rebuild_available_functions() {
  variable_del(REST_CK_AVAILABLE_FUNCTIONS);
  _rest_get_available_functions();
}

/*
 * Look up the set of available functions.
 * This gets all "user" (non-internal) functions,
 * and sorts them alphabetically.
 * 
 * @return the array of types (content type id => specification)
 */
function __rest_lookup_available_functions() {
  $defined_functions = get_defined_functions();
  $user_functions = $defined_functions['user'];
  sort($user_functions, SORT_STRING);
  $user_functions = array_filter($user_functions, create_function('$var', 'return strpos($var, "_") != 0;'));
  return array_combine(array_values($user_functions), array_values($user_functions));
}
