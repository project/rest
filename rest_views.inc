<?php
/**
 * @see hook_views_tables()
 */
function rest_views_tables() {
  return array(
    'rest_functions' => array(
	    'name' => 'rest_functions',
	    'join' => array(
	      'type' => 'inner',
  			'left' => array(
	        'table' => 'node',
	        'field' => 'nid',
	      ),
	      'right' => array(
	        'field' => 'nid',
	      ),
	    ),
	  ),
    'rest_content_types' => array(
	    'name' => 'rest_content_types',
	    'join' => array(
	      'type' => 'inner',
  			'left' => array(
	        'table' => 'node',
	        'field' => 'nid',
	      ),
	      'right' => array(
	        'field' => 'nid',
	      ),
	    ),
	  ),
	);
}

/**
 * @see hook_views_default_views()
 */
function rest_views_default_views() {
  // REST Functions view
  $view = new stdClass();
  $view->name = 'rest_functions';
  $view->description = 'REST Functions';
  $view->access = REST_PERM_ADMIN;
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'REST Functions';
  $view->page_header = '';
  $view->page_header_format = '2';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'No REST Functions defined.';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'rest/functions';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '90';
  $view->menu = TRUE;
  $view->menu_title = 'Functions';
  /*
  $view->menu_tab = TRUE;
  $view->menu_tab_default = FALSE;
  $view->menu_weight = '0';
	*/
  $view->sort = array();
  $view->argument = array();
  $view->field = array(
    array(
      'options' => '[Edit]',
      'handler' => 'views_handler_node_edit',
    ),
    array(
      'tablename' => 'node',
      'field' => 'nid',
      'label' => 'Feature Set',
      'handler' => 'rest_views_handler_function_feature_set',
      'sortable' => '1',
      'options' => 'link',
    ),
    array(
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Description',
      'handler' => 'views_handler_field_nodelink',
      'sortable' => '1',
      'options' => 'link',
    ),
    array(
      'tablename' => 'rest_functions',
      'field' => 'method',
      'label' => 'Method',
      'sortable' => '1',
    ),
    array(
      'tablename' => 'rest_functions',
      'field' => 'request_uri',
      'label' => 'Request URI',
      'sortable' => '1',
    ),
    array(
      'tablename' => 'rest_functions',
      'field' => 'parameters',
      'label' => 'Parameters',
      'handler' => 'rest_views_handler_abbreviate_parameters',
      'sortable' => '1',
    ),
    array(
      'tablename' => 'node',
      'field' => 'nid',
      'label' => 'Content Types',
      'handler' => 'rest_views_handler_function_content_types',
      'sortable' => '1',
    ),
  );
  $view->filter = array();
  $view->exposed_filter = array();
  $view->requires = array(node, rest_functions);
  $views[$view->name] = $view;
  
  // REST Content Types view
  $view = new stdClass();
  $view->name = 'rest_content_types';
  $view->description = 'REST Content Types';
  $view->access = REST_PERM_ADMIN;
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'REST Content Types';
  $view->page_header = '';
  $view->page_header_format = '2';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'No Content Types defined.';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'rest/content_types';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '90';
  $view->menu = TRUE;
  $view->menu_title = 'Content Types';
  /*
  $view->menu_tab = TRUE;
  $view->menu_tab_default = FALSE;
  $view->menu_weight = '0';
	*/
  $view->sort = array();
  $view->argument = array();
  $view->field = array(
    array(
      'options' => '[Edit]',
      'handler' => 'views_handler_node_edit',
    ),
    array(
      'tablename' => 'rest_content_types',
      'field' => 'specification',
      'label' => 'Specification',
      'sortable' => '1',
      'handler' => 'views_handler_field_nodelink',
      'options' => 'link',
    ),
    array(
      'tablename' => 'rest_content_types',
      'field' => 'theme',
      'label' => 'Theme',
      'sortable' => '1',
    ),
    array(
      'tablename' => 'rest_content_types',
      'field' => 'weight',
      'label' => 'Weight',
      'sortable' => '1',
      'defaultsort' => 'ASC',
    ),
  );
  $view->filter = array();
  $view->exposed_filter = array();
  $view->requires = array(node, rest_content_types);
  $views[$view->name] = $view;
  
  return $views;
}

/**
 * Returns the feature set that a function belongs to.
 *
 * @param $value the nid of the function
 * @return the name of the feature set
 */
function rest_views_handler_function_feature_set($fieldinfo, $fielddata, $value, $data) {
  if ($value) {
    return _rest_get_function_feature_set($value);
  }
}

/**
 * Returns a list of the content types that a function handles.
 *
 * @param $value the nid of the function
 * @return the content types
 */
function rest_views_handler_function_content_types($fieldinfo, $fielddata, $value, $data) {
  if ($value) {
    return implode(', ', array_values(rest_get_function_content_types($value)));
  }
}

/**
 * Returns an abbreviated list of a function's parameters.
 * 
 * @param $value the parameters as a serialized array
 * @return a readable version of the list of parameters
 */
function rest_views_handler_abbreviate_parameters($fieldinfo, $fielddata, $value, $data) {
  if ($value) {
    return implode(', ', _rest_abbreviate_parameters(unserialize($value)));
  }
}